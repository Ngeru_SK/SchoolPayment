﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Places.Inputs
{
    public class genericInput
    {
        public String studentId { get; set; }

        public String API_KEY { get; set; }

        public double amount { get; set; }

    }
}
