﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Places.models
{
    public partial class Db : DbContext
    {
        public Db()
        {
        }

        public Db(DbContextOptions<Db> options)
            : base(options)
        {
        }

        public virtual DbSet<TBLAuth> TBLAuth { get; set; }
        public virtual DbSet<payment> payment { get; set; }
        public virtual DbSet<students> students { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-1FRES03\\SQLEXPRESS;Database=TestDB;User ID=sa;Password=123mpaka8;Trusted_Connection=True;Integrated Security=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TBLAuth>(entity =>
            {
                entity.Property(e => e.reg_time)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.token)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<payment>(entity =>
            {
                entity.Property(e => e.reg_time)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.student_id)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.transaction_no)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<students>(entity =>
            {
                entity.Property(e => e.admission_date).HasColumnType("datetime");

                entity.Property(e => e.fee_due).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.fee_paid).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.parents_phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.reg_time)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.student_id)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.student_name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
