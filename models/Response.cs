﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolBankMgt.models
{
    public class Response
    {
        public int statusCode { get; set; }
        public string message { get; set; }
    }
}
