﻿using System;
using System.Collections.Generic;

namespace Places.models
{
    public partial class TBLAuth
    {
        public int id { get; set; }
        public string token { get; set; }
        public DateTime? reg_time { get; set; }
    }
}
