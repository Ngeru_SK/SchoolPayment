﻿using System;
using System.Collections.Generic;

namespace Places.models
{
    public partial class payment
    {
        public int id { get; set; }
        public string transaction_no { get; set; }
        public double amount { get; set; }
        public string student_id { get; set; }
        public DateTime? reg_time { get; set; }
    }
}
