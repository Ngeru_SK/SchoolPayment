﻿using System;
using System.Collections.Generic;

namespace Places.models
{
    public partial class students
    {
        public int id { get; set; }
        public string student_id { get; set; }
        public string student_name { get; set; }
        public string parents_phone { get; set; }
        public DateTime? admission_date { get; set; }
        public DateTime? reg_time { get; set; }
        public decimal? fee_due { get; set; }
        public decimal? fee_paid { get; set; }
    }
}
