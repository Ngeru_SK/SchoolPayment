﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Places.Inputs;
using Places.models;
using SchoolBankMgt.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Places.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class StudentController : ControllerBase
    {

        [HttpGet]
        public String IntroFunction()
        {

            return "All Set. The rest are post methods";
        }

        [HttpPost]
        public List<students> FetchAllStudents(genericInput studentmodel) {

            List<students> allstudents = new List<students>();
            if(IsTokenValid(studentmodel.API_KEY))
            {
                try {
                using (Db db = new Db()) {

                    var studentlist = db.students.ToList();
                    allstudents = studentlist;


            }
            } catch (Exception e) { 
            //log error
            }
            }
         

            return allstudents;
        }


        [HttpPost]
        public students FetchStudent(genericInput studentmodel)
        {
            var newstudent = new students();
            if (IsTokenValid(studentmodel.API_KEY))
            {
                try
                {

                    using (Db db = new Db())
                    {
                        newstudent = db.students.Where(x => x.student_id == studentmodel.studentId).FirstOrDefault();
                    }
                }
                catch (Exception e)
                {
                    //log error
                }
            }

            return newstudent;
        }

        [HttpPost]
        public async Task<Response> PostPaymentAsync(genericInput studentmodel)
        {
            Response resp = new Response();

            if (IsTokenValid(studentmodel.API_KEY))
            {
                try
                {
                    if (studentmodel.amount < 1 || studentmodel.studentId == null)
                    {
                        resp.statusCode = 400;
                        resp.message = "Validation Failed";
                        return resp;
                    }

                    using (Db db = new Db())
                    {
                        payment pm = new payment();
                        pm.transaction_no = "SP_V"+ GetTimestamp(DateTime.Now);
                        pm.amount = studentmodel.amount;
                        pm.student_id = studentmodel.studentId;
                        db.payment.Add(pm);
                        await db.SaveChangesAsync();

                        resp.statusCode = 200;
                        resp.message = "Saved Successfully";
                    }
                }
                catch (Exception e)
                {
                    //log error
                    resp.statusCode = 200;
                    resp.message = "Saved Successfully";
                }
            }

            return resp;
        }


        public bool IsTokenValid(String authToken)
        {
            bool vaid = false;
            try
            {
                using (Db db = new Db())
                 {
                    var m_token = db.TBLAuth.Where(x=>x.token == authToken).FirstOrDefault();
                    if(m_token != null)
                    {
                        return true;
                    }
                 /*   foreach(var mm in all_tokens)
                    {
                        if(mm.token.Equals(token))
                        {
                            return true;

                        }
                    } */
                }

            }catch(Exception e)
            {
                //log error
            }


            return vaid;
        }
        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }
        //


    }
}
